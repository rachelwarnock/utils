# throw error if input file not specified
if [ -z "$1" ]
  then echo "Specify an input file!"
  exit 1
fi

# remove the last line of the file
sed '$ d' $1 > tmp.log
# open temporary file in tracer
java -jar /Applications/Tracer\ v1.6.0.app/Contents/Resources/Java/tracer.jar tmp.log
# remove temporary files
rm tmp.log

